# README #


## What this repo is for ##

This is for web based projects. It is a script that animates in your website contents as you scroll down the page.

## How to set up ##

Within the <head> tags of your html page, include the following two files accordingly:
	1. jquery-3.2.1.js
	2. urch_slider.js
That is, include the jquery library, then include urch_slider.js script. That's all.

## Usage ##

Any element in your html you want to animate in style as you scroll to its position, add any one of the following classes:
	1. appear-normal
	2. appear-north
	3. appear-south
	4. appear-east
	5. appear-west
	
In HTML:
	<div class="appear-normal">
		<h5>This div would be hidden initially, but as I scroll down to where this div is positioned, It will animate in great style</h5>
	</div>
In JS:
	$('.selected-div').addClass('appear-normal');

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
